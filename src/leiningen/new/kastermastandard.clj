(ns leiningen.new.kastermastandard
  (:use [leiningen.new.templates :only [renderer name-to-path ->files]]))

(def render (renderer "kastermastandard"))

(defn kastermastandard
  "standard project setup for kasterma"
  [name]
  (let [data {:name name
              :sanitized (name-to-path name)}]
    (->files data
             ["src/{{sanitized}}/core.clj" (render "core.clj" data)]
             ["dev/user.clj" (render "user.clj" data)]
             ["project.clj" (render "project.clj" data)]
             [".travis.yml" (render "dottravis.yml" data)]
             ["test/{{sanitized}}/coretest.clj" (render "coretest.clj" data)])))
