(defproject {{name}} "0.1.0-SNAPSHOT"
  :description "PROVIDE DESCRIPTION"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0-alpha1"]
                 [org.clojure/tools.cli "0.3.1"]
                 [com.taoensso/timbre "3.3.1"]
                 [incanter "1.5.5"]
                 [org.clojure/core.logic "0.8.9"]
                 [org.clojure/algo.monads "0.1.5"]
                 [instaparse "1.3.4"]
                 [prismatic/plumbing "0.3.4"]
                 [prismatic/schema "0.3.1"]]

  :jvm-opts ["-XX:MaxPermSize=128M"
             "-XX:+UseConcMarkSweepGC"
             "-Xms2g" "-Xmx2g" "-server"]

  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]]

  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.7"]
                                  [org.clojure/tools.trace "0.7.8"]
                                  [midje "1.6.3"
                                   :exclusions [joda-time
                                                commons-codec]]]
                   :source-paths ["dev"]}}

  :repl-options {:init (user/go)})
