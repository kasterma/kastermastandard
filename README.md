# kastermastandard

A Leiningen template for kasterma containing his standard settings
(that is a set of libs etc he uses that gets pruned down per project).

## Usage

lein new kastermastandard &lt;project name&gt;

## License

Copyright © 2013

Distributed under the Eclipse Public License, the same as Clojure.
